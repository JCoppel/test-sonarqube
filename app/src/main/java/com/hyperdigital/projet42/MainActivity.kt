package com.hyperdigital.projet42

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.SideEffect
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import com.google.accompanist.systemuicontroller.rememberSystemUiController
import com.hyperdigital.projet42.domain.model.EventShort
import com.hyperdigital.projet42.ui.components.BottomNavigationBar
import com.hyperdigital.projet42.ui.components.EventList
import com.hyperdigital.projet42.ui.theme.BackgroundPink
import com.hyperdigital.projet42.ui.theme.Projet42Theme
import com.hyperdigital.projet42.ui.components.HeadSection

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            Projet42Theme {
                SetBarColor(color = BackgroundPink)

                // NavController
                val navController = rememberNavController()

                Scaffold(
                    bottomBar = {
                        BottomNavigationBar(navController)
                    }
                ) { padding ->
                    Surface(
                        modifier = Modifier
                            .fillMaxSize()
                            .padding(padding),
                        color = BackgroundPink
                    ) {
                        NavigationComponent(navController)
                    }
                }
            }
        }
    }

    @Composable
    private fun SetBarColor(color: Color) {
        val systemUiController = rememberSystemUiController()
        SideEffect {
            systemUiController.setSystemBarsColor(
                color = color
            )
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun HomeScreen() {
    Column(
        modifier = Modifier
            .fillMaxSize()
    ) {
        HeadSection()
        //        EventSection()
        Spacer(modifier = Modifier.height(16.dp))
        //        CategorySection()
        //        LastMinEvent()
    }
}


@Composable
fun NavigationComponent(navController: NavHostController) {
    NavHost(navController = navController, startDestination = "home") {
        composable("home") {
            HomeScreen()
        }
        composable("events") {
            EventList(
                events = listOf(
                    EventShort(
                        eventId = "1",
                        title = "test Album",
                        imageEvent = "https://www.kasandbox.org/programming-images/avatars/leaf-blue.png",
                        date = "2021-10-10"
                    ),
                    EventShort(
                        eventId = "2",
                        title = "Sample Album",
                        imageEvent = "https://www.kasandbox.org/programming-images/avatars/leaf-green.png",
                        date = "2022-10-10"
                    ),
                    EventShort(
                        eventId = "3",
                        title = "second Album",
                        imageEvent = "https://www.kasandbox.org/programming-images/avatars/leaf-grey.png",
                        date = "2023-10-10"
                    )
                )
            )

        }
    }
}