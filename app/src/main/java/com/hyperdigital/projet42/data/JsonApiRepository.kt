//package com.hyperdigital.projet42.data
//
//import com.juliencoppel.photos.data.api.JsonApi
//import com.juliencoppel.photos.data.api.model.JsonApiUserShort
//import com.juliencoppel.photos.domain.model.AlbumShort
//import com.juliencoppel.photos.domain.model.PhotoShort
//import com.juliencoppel.photos.domain.model.UserShort
//import com.juliencoppel.photos.domain.repository.UserRepository
//import okhttp3.OkHttpClient
//import retrofit2.Retrofit
//import retrofit2.converter.gson.GsonConverterFactory
//
//private const val BASE_URL = "https://jsonplaceholder.typicode.com"
//
//class JsonApiRepository : UserRepository {
//
//    private val api: JsonApi
//
//    init {
//        val okhttpClient = OkHttpClient.Builder().build()
//
//        val retrofit: Retrofit = Retrofit.Builder()
//            .baseUrl(BASE_URL)
//            .client(okhttpClient)
//            .addConverterFactory(GsonConverterFactory.create())
//            .build()
//
//        api = retrofit.create(JsonApi::class.java)
//    }
//
//    override suspend fun usersList(): Result<List<UserShort>> {
//        return try {
//            val result = api.getUsers()
//
//            Result.success(result)
//        } catch (e: Exception) {
//            Result.failure(e)
//        }
//    }
//
//    override suspend fun userAlbumsList(userId: String): Result<List<AlbumShort>> {
//        return try {
//            val result = api.getAlbums(userId)
//
//            Result.success(result)
//        } catch (e: Exception) {
//            Result.failure(e)
//        }
//    }
//
//    override suspend fun userPhotosList(albumId: String): Result<List<PhotoShort>> {
//        return try {
//            val result = api.getPhotos(albumId)
//
//            Result.success(result)
//        } catch (e: Exception) {
//            Result.failure(e)
//        }
//    }
//}