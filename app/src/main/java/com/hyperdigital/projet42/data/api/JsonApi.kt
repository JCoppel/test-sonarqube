//package com.hyperdigital.projet42.data.api
//
//import com.juliencoppel.photos.domain.model.AlbumShort
//import com.juliencoppel.photos.domain.model.PhotoShort
//import com.juliencoppel.photos.domain.model.UserShort
//import retrofit2.http.GET
//import retrofit2.http.Query
//
//interface JsonApi {
//
//    @GET("/users")
//    suspend fun getUsers(): List<UserShort>
//
//    @GET("/albums")
//    suspend fun getAlbums(
//        @Query("userId") userId: String
//    ): List<AlbumShort>
//
//    @GET("/photos")
//    suspend fun getPhotos(
//        @Query("albumId") albumId: String
//    ): List<PhotoShort>
//
//}