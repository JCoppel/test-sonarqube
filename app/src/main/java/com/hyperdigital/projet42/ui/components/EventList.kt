package com.hyperdigital.projet42.ui.components

import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.hyperdigital.projet42.domain.model.EventShort
import com.hyperdigital.projet42.ui.theme.Projet42Theme

@Composable
fun EventList(
    events: List<EventShort>,
    modifier: Modifier = Modifier,
    onItemClick: (eventId: String) -> Unit = {}
) {
    val listState = rememberLazyListState()

    LazyColumn(
        modifier = modifier,
        state = listState
    ) {
        itemsIndexed(events) { _, item ->

            EventItem(
                eventShort = item,
                onItemClick = onItemClick
            )

            Spacer(modifier = Modifier.height(10.dp))
        }
    }
}

@Preview(showBackground = true)
@Composable
fun EventListPreview() {
    Projet42Theme {
        val events = listOf(
            EventShort(
                eventId = "1",
                title = "Sample Album",
                imageEvent = "https://www.kasandbox.org/programming-images/avatars/leaf-blue.png",
                date = "2021-10-10"
            ),
            EventShort(
                eventId = "1",
                title = "Sample Album",
                imageEvent = "https://www.kasandbox.org/programming-images/avatars/leaf-green.png",
                date = "2021-10-10"
            ),
            EventShort(
                eventId = "1",
                title = "Sample Album",
                imageEvent = "https://www.kasandbox.org/programming-images/avatars/leaf-grey.png",
                date = "2021-10-10"
            )
        )

        EventList(events)
    }
}
