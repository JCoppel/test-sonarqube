package com.hyperdigital.projet42.ui.component

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.OutlinedTextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.hyperdigital.projet42.R

@Composable
fun SearchHeader(
    modifier: Modifier = Modifier,
    onSearchClick: (text: String) -> Unit = {}
) {

    var text by remember { mutableStateOf("") }

    Row(
        modifier = modifier,
        verticalAlignment = Alignment.CenterVertically
    ) {
        OutlinedButton(
            modifier = Modifier,
            onClick = { onSearchClick(text) }
        ) {
            Image(
                painter = painterResource(id = R.drawable.ic_search),
                contentDescription = "Search"
            )
        }
        OutlinedTextField(
            modifier = Modifier
                .weight(1.0f)
                .padding(end = 4.dp),
            value = text,
            onValueChange = { text = it },
        )
    }
}

@Preview(showBackground = true)
@Composable
fun SearchHeaderPreview() {
    MaterialTheme {
        SearchHeader()
    }
}
