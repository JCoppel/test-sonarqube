package com.hyperdigital.projet42.ui.components

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Row
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.AccountCircle
import androidx.compose.material.icons.rounded.Event
import androidx.compose.material.icons.rounded.Home
import androidx.compose.material.icons.rounded.Notifications
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.NavigationBar
import androidx.compose.material3.NavigationBarItem
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.navigation.NavHostController
import com.hyperdigital.projet42.data.BottomNavigation


val items = listOf(
    BottomNavigation(
        title = "Home",
        icon = Icons.Rounded.Home
    ),
    BottomNavigation(
        title = "Events",
        icon = Icons.Rounded.Event
    ),
    BottomNavigation(
        title = "Notifications",
        icon = Icons.Rounded.Notifications
    ),
    BottomNavigation(
        title = "Account",
        icon = Icons.Rounded.AccountCircle
    ),
)

@Composable
fun BottomNavigationBar(navController: NavHostController) {
    var selectedIndex by remember { mutableStateOf(0) } // Ajoutez cette ligne

    NavigationBar {
        Row(
            //couleur de la bar a changer
            modifier = Modifier.background(MaterialTheme.colorScheme.inverseOnSurface)
        ) {
            items.forEachIndexed { index, item ->
                NavigationBarItem(selected = index == selectedIndex,
                    onClick = {
                        selectedIndex = index
                        when (item.title) {
                            "Home" -> navController.navigate("home")
                            "Events" -> navController.navigate("events")
                            // Ajoutez d'autres cas pour les autres icônes si nécessaire
                        }
                    },
                    icon = {
                        Icon(
                            imageVector = item.icon,
                            contentDescription = item.title,
                            //la "tint" est la couleur du background de l'icone ici elle sera de la couleur du background de l'ecran d'accueil du telephone"onBackground"
                            tint = MaterialTheme.colorScheme.onBackground
                        )
                    },
                    label = {
                        Text(
                            text = item.title,
                            color = MaterialTheme.colorScheme.onBackground
                        )
                    }
                )
            }
        }
    }
}