package com.hyperdigital.projet42.ui.components

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Card
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import coil.compose.rememberAsyncImagePainter
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.ui.layout.ContentScale
import com.hyperdigital.projet42.domain.model.EventShort
import com.hyperdigital.projet42.ui.theme.Pink80
import com.hyperdigital.projet42.ui.theme.Projet42Theme

@Composable
fun EventItem(
    eventShort: EventShort,
    modifier: Modifier = Modifier,
    onItemClick: (eventId: String) -> Unit = {}
) {
    Card(
        modifier = modifier
            .height(275.dp)
            .padding(8.dp)
            .fillMaxSize(),
        //.clickable { onItemClick.invoke(eventShort.id) },
        shape = MaterialTheme.shapes.medium
    ) {
        Column(
            modifier = Modifier.fillMaxSize(),
        ) {
            Image(
                painter = rememberAsyncImagePainter(model = eventShort.imageEvent),
                contentDescription = null,
                modifier = Modifier
                    .height(150.dp)
                    .fillMaxWidth(),
                contentScale = ContentScale.Crop
            )
            Row (
                modifier = Modifier.padding(8.dp)
            ){
                Text(text = eventShort.title)
                Spacer(modifier = Modifier.weight(1f))
                Text(text = eventShort.date)
            }
            //Ajout d'un button qui prend tous l'ecran et de couleur rose
            Button(
                onClick = { /* Do something! */ },
                modifier = Modifier.fillMaxWidth()
                    .padding(10.dp
                    ),
                colors = ButtonDefaults.buttonColors(
                    containerColor = Pink80
                )
            ) {
                Text("S'inscrire à l'événements")
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun EventItemPreview() {
    Projet42Theme {
        val eventShort = EventShort(
            eventId = "1",
            title = "Sample Album",
            imageEvent = "https://www.kasandbox.org/programming-images/avatars/leaf-blue.png",
            date = "2021-10-10"
        )

        EventItem(eventShort)
    }
}