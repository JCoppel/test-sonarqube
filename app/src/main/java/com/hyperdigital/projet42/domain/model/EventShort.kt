package com.hyperdigital.projet42.domain.model

import java.util.Date

data class EventShort(
    val eventId: String,
    val title: String,
    val imageEvent: String,
    val date: String
)