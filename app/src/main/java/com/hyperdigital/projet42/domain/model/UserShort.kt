package com.hyperdigital.projet42.domain.model

data class UserShort(
    val id: String,
    val name: String,
    val website: String
)
