package com.hyperdigital.projet42.domain.repository//package com.hyperdigital.projet42.domain.repository
//
//import com.juliencoppel.photos.domain.model.AlbumShort
//import com.juliencoppel.photos.domain.model.PhotoShort
//import com.juliencoppel.photos.domain.model.UserShort
//
//interface UserRepository {
//    suspend fun usersList(): Result<List<UserShort>>
//    suspend fun userAlbumsList(userId: String): Result<List<AlbumShort>>
//    suspend fun userPhotosList(albumId: String): Result<List<PhotoShort>>
//}