package com.hyperdigital.projet42.domain.model

data class PhotoShort(
    val albumId: String,
    val id: String,
    val title: String,
    val url: String,
    val thumbnailUrl: String
)

